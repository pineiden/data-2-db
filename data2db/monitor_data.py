from pathlib import Path
import re
from datetime import datetime, timedelta


class MonitorPath:
    def __init__(self,
                 directory,
                 extension,
                 conversion,
                 last_time,
                 *args, **kwargs):
        self._dir = Path(directory)
        self._ext = extension
        self._conversion = conversion
        self._regex = f"*\.{self._ext}$"
        self._last_time = timedelta(**last_time)
        self._processed = []

    def added_files(self):
        with open('processed.log') as f:
            reader = f.readlines()
            for line in reader:
                filepath = Path(line)
                if filepath.exists():
                    self._processed.append(filepath)

    def check(self):
        """
        Solo toma archivos nuevos antes de '_last_time'
        """
        now = datetime.utcnow()
        for filename in self._dir.glob(self._regex):
            if filename.exists():
                mtime = datetime.datetime.fromtimestamp(
                    filename.stat().st_mtime)
                print(filename, type(filename), filename.stat())
                if now-self._last_time >= mtime:
                    print("Leyendo archivo")
                    print("Guardando a database")
                    self.procesar(filename)

    def procesar(self, filename):
        self._conversion(filename)
