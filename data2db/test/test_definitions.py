from data2db.definitions import build_table
from data2db.dynamodb import DynamoDB
from pprint import pprint

host = "https://dynamodb.us-east-2.amazonaws.com"
kwargs = {"code": "all", "class_name": "dynamodb",
          "hostname": "localhost:8042"}

region="us-east-2"

if __name__=='__main__':
    table_name = "Prueba"
    fields = [
        ("datetime", "numeric", "partition"),
        ("valor1", "numeric", ""),
    ]
    new_table = build_table(table_name, fields)
    
    pprint(new_table)
    
    dynamodb = DynamoDB(
        url=host,
        region=region,
        **kwargs)
    
    tables = dynamodb.list_tables()
    pprint(tables)

    dynamodb.create_table(**new_table)
    
    tables = dynamodb.list_tables()
    pprint(tables)
