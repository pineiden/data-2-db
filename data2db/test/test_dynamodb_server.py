from data2db.dynamodb import DynamoDB
from pprint import pprint

host = "https://dynamodb.us-east-2.amazonaws.com"
kwargs = {"code": "all", "class_name": "dynamodb",
          "hostname": "localhost:8042"}

region="us-east-2"
dynamodb = DynamoDB(
    url=host,
    region=region,
    **kwargs)
tables = dynamodb.list_tables()
pprint(tables)
