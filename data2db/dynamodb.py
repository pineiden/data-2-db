from .general import GenericDB
import boto3

#from .params import ACCESS, SECRET


class DynamoDB(GenericDB):
    def __init__(self, url, region, *args, **kwargs):
        credentials = kwargs.get("credentials")
        ACCESS = credentials.get("ACCESS_KEY")
        SECRET = credentials.get("SECRET")
        session = boto3.Session(
		    aws_access_key_id=ACCESS,
		    aws_secret_access_key=SECRET)
        self.client =  boto3.client("dynamodb",
                                    region_name=region,
                                    endpoint_url=url)
        # cliente: https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/connect.html
        self.resource =  boto3.resource("dynamodb",
                                    region_name=region,
                                    endpoint_url=url)
        # https://docs.aws.amazon.com/es_es/amazondynamodb/latest/developerguide/GettingStarted.Python.02.html
        super().__init__(*args,**kwargs)

    def table_is_active(self, table_name):
        table = self.tables.get(table_name)
        return table.stable_status == 'ACTIVE'

    def list_tables(self):
        table_names = self.client.list_tables().get("TableNames")
        self.tables = {name: self.resource.Table(name) for name in table_names}
        return self.tables

    def create_table(self, **kwargs):
        """
        Se debe validar el diccionario con DescribeTable, una vez creada
        esperar un poco
        Ejemplo:

table = dynamodb.create_table(
            TableName='users',
            KeySchema=[
                {
                    'AttributeName': 'username',
                    'KeyType': 'HASH'
                },
                {
                    'AttributeName': 'last_name',
                    'KeyType': 'RANGE'
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'username',
                    'AttributeType': 'S'
                },
                {
                    'AttributeName': 'last_name',
                    'AttributeType': 'S'
                },
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }
        )
        """
        table_name = kwargs.get("TableName")
        print("Tables", self.tables.keys(), table_name, table_name not in self.tables)

        if table_name not in self.tables:
            table = self.client.create_table(**kwargs)
            #table.meta.client.get_waiter(
            #    "table_exists").wait(
            #        TableName=table_name)
            self.tables[table_name] = self.resource.Table(table_name)
            return table

    def update_table(self,table_name, **kwargs):
        if table_name not in self.tables:
            table = self.client.update_table(**kwargs)
            return table

    def save_data(self, table_name, dataset):
        """
        dataset must be a json array ([{},{},{}])
        """
        table = self.tables.get(table_name)
        if not table:
             table = self.resource.Table(table_name)
             self.tables[table_name] = table
        for data in dataset:
            table.put_item(Item=data)


    def get_data(self, key, start, end):
        pass
