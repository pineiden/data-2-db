def attribute_definition(name:str, data_type:str='string'):
    types =  {
        "string":"S",
        "numeric":"N",
        "binary":"B"
    }
    definition = {
            'AttributeName': name,
            'AttributeType': types.get(data_type, 'S')
        }
    return definition


def keys_definition(name:str, key_type:str='partition'):
    types =  {
        "partition":"HASH",
        "sort":"RANGE"
    }
    definition = {
            'AttributeName': name,
            'KeyType': types.get(key_type, 'RANGE')
        }
    return definition

def provisioned_definition(read:int=5, write:int=5):
    definition=  {
                'ReadCapacityUnits': read,
                'WriteCapacityUnits': write
            }
    return definition

def build_table(table_name, fields, **kwargs):
    """
    fields :: array defining the keyschema
    """
    read_capacity = kwargs.get("read_capacity",5)
    write_capacity = kwargs.get('write_capacity',5)
    capacities = [read_capacity,write_capacity]
    new_table = dict(
        TableName=table_name,
        AttributeDefinitions=[attribute_definition(key,data_type=datatype) for key, datatype, _ in fields],
        KeySchema=[keys_definition(key,key_type=ftype) for key, _, ftype in fields],
        ProvisionedThroughput=provisioned_definition(*capacities)
    )
    return new_table
    
                
