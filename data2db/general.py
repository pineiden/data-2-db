from basic_logtools.filelog import LogFile


class GenericDB:
    def __init__(self, *args, **kwargs):
        self.credentials = kwargs.get('credentials')
        self.host = kwargs.get('host')
        self.port = kwargs.get('port')
        log_level = kwargs.get('loglevel', "INFO")
        log_path = kwargs.get("logpath", "./")
        self.logger = LogFile(
            kwargs.get("class_name", "genericdb"),
            kwargs.get("code", "test"),
            kwargs.get("hostname", "localhost"),
            path=log_path,
            max_bytes=51200000,
            base_level=log_level)

    def connect(self):
        """
        Crea conexión sincrona, asocia _conn
        """
        pass

    async def async_connect(self):
        """
        Crea conexion asincrona, asocia a _conn
        """
        pass

    def list_databases(self):
        """
        list databases names
        """
        self._dbs = []

    def list_tables(self):
        """
        list the tables
        """
        self._tables = []

    def create_db(self, name):
        if name not in self._dbs:
            self.__conn.create_db(name)

    def save_data(self, table, data):
        """
        save the data to table
        """
        if table in self._tables:
            if self.validate(data):
                try:
                    result = self._conn.save(data)
                    return result
                except Exception as ex:
                    self.logger.exception(ex)
                    raise ex
            else:
                return {}
        else:
            return {}
