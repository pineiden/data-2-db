from setuptools import setup
from pathlib import Path

path = Path(__file__).resolve().parent

with open(path/'README.md', encoding='utf-8') as f:
    long_description = f.read()

with open(path/'VERSION') as version_file:
    version = version_file.read().strip()


setup(name='data2db',
      version=version,
      description='Data to database tools',
      url='https://gitlab.com/pineiden/data-2-db',
      author='David Pineda Osorio',
      author_email='dpineda@csn.uchile.cl',
      license='MIT',
      packages=['data2db'],
      keywords = ["nosql","databases",],
      install_requires=["networktools",
                        "tasktools",
                        "click",
                        "boto3",
                        "ujson",
                        "basic-logtools",],
      entry_points={
        },
      include_package_data=True,
      long_description=long_description,
      long_description_content_type='text/markdown',
      zip_safe=False)
