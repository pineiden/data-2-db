from data2db.dynamodb import DynamoDB
from pprint import pprint

host = "http://localhost:8042"
kwargs = {"code": "all", "class_name": "dynamodb",
          "hostname": "localhost:8042","credentials":{}}
dynamodb = DynamoDB(
    url=host,
    region="chile", **kwargs)
tables = dynamodb.list_tables()
pprint(tables)
