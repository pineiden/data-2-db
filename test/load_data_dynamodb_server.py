from decimal import Decimal
import json
import boto3
from data2db.dynamodb import DynamoDB
from pprint import pprint

host = "https://dynamodb.us-east-2.amazonaws.com"
kwargs = {"code": "all", "class_name": "dynamodb",
          "hostname": "localhost:8042"}

region="us-east-2"

"""
create table
"""

params=dict(TableName='32681',
            KeySchema=[
                {
                    'AttributeName': 'datetime',
                    'KeyType': 'HASH'
                },
                {
                    'AttributeName': 'value',
                    'KeyType': 'RANGE'
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'datetime',
                    'AttributeType': 'S'
                },
                {
                    'AttributeName': 'value',
                    'AttributeType': 'N'
                },
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 5,
                'WriteCapacityUnits': 5
            }                
            )


if __name__ == '__main__':
    dataset = []
    with open("32681.txt") as txt:
        for line in txt.readlines():
            dataset.append(json.loads(line, parse_float=Decimal))

    pprint(dataset)

    dynamodb = DynamoDB(
        url=host,
        region=region,
        **kwargs)
    tables = dynamodb.list_tables()
    pprint(tables)

    dynamodb.create_table(**params)
    pprint(params)
    table_name=params.get("TableName")
    dynamodb.save_data(table_name, dataset)
